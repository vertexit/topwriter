@if(in_array($article->status, ['review', 'approved-edited']))
    <a href="{{ route('articles.edit', ['article' => $article]) }}">
        <button class='approve-theme btn btn-outline-primary' data-id="{{ $article->id }}">
            <i class='os-icon os-icon-edit-32'></i> Review
        </button>
    </a>
@elseif(in_array($article->status, ['approved', 'rejected']))
    <a href="#">
        <button class='btn btn-outline-primary' data-id="{{ $article->id }}">
            <i class='os-icon os-icon-edit-32'></i> Read Article
        </button>
    </a>
@endif
