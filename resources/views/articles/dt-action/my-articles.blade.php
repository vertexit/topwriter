<a href="{{ route('articles.edit-after-published', ['article' => $article]) }}">
    <button class='btn btn-outline-primary' data-id="{{ $article->id }}">
        <i class='os-icon os-icon-edit-32'></i> Edit
    </button>
</a>
