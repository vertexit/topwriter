<div class="element-box">
    <form action="#" method="POST">
        <h5 class="form-header">Document meta</h5>
        <div class="form-desc">
            Suggest new theme and editor will decide if you will write it
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name"> Article name</label>
                    <input class="form-control" id="name" name="name" placeholder="Enter article name" type="text" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name"> Article name</label>
                    <input class="form-control" id="name" name="name" placeholder="Enter article name" type="text" required>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="category"> Meta Description</label>
            <textarea class="form-control" name="discussion" placeholder="Start the discussion with editor" required></textarea>
        </div>
    </form>
</div>
<div class="element-box" id="seo-images-box" hidden>
    <h5 class="form-header">Images</h5>
    <div class="form-desc">
        Suggest new theme and editor will decide if you will write it
    </div>
    <div class="display-images-container"></div>
    {{-- <div class="row">
        <div class="col-md-12">
            <div class="row mb-4">
                <img class="mx-auto" src="https://timedotcom.files.wordpress.com/2015/05/clint-eastwood.jpg?quality=85" height="300"/>
            </div>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="form-group">
                        <label for=""> Image name</label>
                        <input type="text" class="form-control" name="" id="" placeholder="Image name"/>
                    </div>
                    <div class="form-group">
                        <label for=""> Alt tag</label>
                        <input type="text" class="form-control" name="" id="" placeholder="Alt tag"/>
                    </div>
                    <div class="form-group">
                        <label for=""> Captions</label>
                        <textarea class="form-control" name="" placeholder="Captions" required></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="row mb-4">
                <img class="mx-auto" src="https://s.hswstatic.com/gif/eastwood-rule-1.jpg" height="300"/>
            </div>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="form-group">
                        <label for=""> Image name</label>
                        <input type="text" class="form-control" name="" id="" placeholder="Image name"/>
                    </div>
                    <div class="form-group">
                        <label for=""> Alt tag</label>
                        <input type="text" class="form-control" name="" id="" placeholder="Alt tag"/>
                    </div>
                    <div class="form-group">
                        <label for=""> Captions</label>
                        <textarea class="form-control" name="" placeholder="Captions" required></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="row mb-4">
                <img class="mx-auto" src="https://static.tvtropes.org/pmwiki/pub/images/clinteastwood.jpg" height="300"/>
            </div>
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="form-group">
                        <label for=""> Image name</label>
                        <input type="text" class="form-control" name="" id="" placeholder="Image name"/>
                    </div>
                    <div class="form-group">
                        <label for=""> Alt tag</label>
                        <input type="text" class="form-control" name="" id="" placeholder="Alt tag"/>
                    </div>
                    <div class="form-group">
                        <label for=""> Captions</label>
                        <textarea class="form-control" name="" placeholder="Captions" required></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-2">
            <img src="https://cdn1.thr.com/sites/default/files/imagecache/scale_crop_768_433/2018/03/scott_eastwood_clint_eastwood_split.jpg" width="160"/>
        </div>
        <div class="col-md-10">
            <div class="form-group">
                <label for=""> Image name</label>
                <input type="text" class="form-control" name="" id="" placeholder="Image name"/>
            </div>
            <div class="form-group">
                <label for=""> About Theme</label>
                <input type="text" class="form-control" name="" id="" placeholder="Alt tag"/>
            </div>
            <div class="form-group">
                <label for=""> Captions</label>
                <textarea class="form-control" name="" placeholder="Captions" required></textarea>
            </div>
        </div>
    </div> --}}
</div>
