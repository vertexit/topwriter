<div class="write-element element-box" data-number="{{ $section['order_number'] ?? '1' }}">
    <div class="images" hidden></div>
    <div class="row">
        <h3 class="col font-weight-bold task-order-number">{{ $section['order_number'] ?? '1' }}.</h3>
        <input type="text" class="col col-md-11 task-heading form-control" value="{{ $section['title'] ?? '' }}" placeholder="Task name">
    </div>
    <div class="form-desc"></div>
    <form action="{{ route('write.image-upload', ['theme' => $theme]) }}" class="dropzone form-group">
        @csrf
        <div class="dz-message">
            <div>
                <h4>Drop files here or click to upload.</h4>
                <div class="text-muted">Text about explanation for slider</div>
            </div>
        </div>
    </form>
    <div class="form-group">
        <textarea name="content" id="text-editor-{{ $section['order_number'] ?? '1' }}" class="text-editor form-control">{{ $section['content'] ?? '' }}</textarea>
    </div>
</div>
