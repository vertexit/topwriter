@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div id="content" class="element-wrapper">
            <h6 class="element-header">
                Write Article - <span id="theme-name">{{ $theme->name }}</span>
            </h6>
            <div class="os-tabs-w mx-4">
                <div class="os-tabs-controls">
                    <ul class="nav nav-tabs upper">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" data-action="write" href="#"> Write</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" data-action="seo" href="#"> SEO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" data-action="finish" href="#"> Finish</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="content-page" data-name="write">
                <div id="write">
                    @if($sections)
                        @foreach ($sections as $section)
                            @include('articles.components.write-element', ['section' => $section])
                        @endforeach
                    @else
                        @include('articles.components.write-element')
                    @endif
                </div>
                <button id="new-number" class="btn btn-outline-primary font-weight-bold col-md-12 py-4">Add new number</button>
            </div>
            <div class="content-page" data-name="seo" hidden>
                @include('articles.components.seo-element')
            </div>
            <div class="content-page" data-name="finish" hidden>
                @if(in_array(Route::currentRouteName(), ['write', 'articles.edit-after-published']))
                    @include('articles.components.finish-element-writer')
                @elseif(Route::currentRouteName() == 'articles.edit')
                    @include('articles.components.finish-element-editor')
                @endif
            </div>
        </div>
    </div>
    <div class="content-panel">
        <div class="element-wrapper">
            <h6 class="element-header">Help</h6>
            <div class="element-box"></div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=dwmkysehjuclo8kao1c6js9osjw4zqz2pk9nvuh4bvgbenm0"></script>
<script>
    var tinymceSettings = {
        selector: '.text-editor',
        height: 400,
        theme: 'modern',
        browser_spellcheck: true,
        plugins: [
            'advlist autolink lists link image charmap hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save contextmenu',
            'template paste textcolor textpattern imagetools help'
        ],
        toolbar1: 'undo redo | styleselect | bold italic | link | image media | help',
        image_caption: true,
        // content_css: [
        //     '//fonts.googleapis.com/css?family=Rubik:300,400,500',
        //     '//www.tinymce.com/css/codepen.min.css'
        // ],
        branding: false,
    };

    function attachDropzoneData() {
        var myDropzone = Dropzone.forElement('.dropzone');

        myDropzone.on('sending', function(file, xhr, formData){
            formData.append('theme', $('#theme-name').text());
            formData.append('number', $('.last').data('number'));
        });

        myDropzone.on("success", function (file, response) {
            myDropzone.previewsContainer
                .parentElement
                .firstChild
                .nextElementSibling
                .append(response + "|");

            displayImage(response);
        });
    }

    function initDropzone(element) {
        element.dropzone({
            url: '{{ route('write.image-upload', ['theme' => $theme]) }}',
            params: {
                'theme': $('#theme-name').text(),
                'number': $('.last').data('number'),
            },
            success: function (file, response) {
                this.clickableElements[0]
                    .parentElement
                    .firstChild
                    .append(response + "|");

                displayImage(response);
            },
            error: function () {
                alert('error');
            }
        });
    }

    // TODO: proslijediti order number
    function displayImage(image) {
        var array = image.split('/');
        var nameAndPath = array[6].split('.');
        var name = convertToSlug(nameAndPath[0]);

        $('#seo-images-box').attr('hidden', false);

        $('.display-images-container').append(
            '<div class="row images-seo-box" data-order="">' +
                '<input type="hidden" class="seo-image-name-original" value="' + name + '">' +
                '<div class="col-md-12">' +
                    '<div class="row mb-4">' +
                        '<img class="mx-auto" src="' + image + '" height="300"/>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-md-6 mx-auto">' +
                            '<div class="form-group">' +
                                '<label for=""> Image name</label>' +
                                '<input type="text" class="form-control seo-image-name" placeholder="Image name" value="' + name + '"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label for=""> Alt tag</label>' +
                                '<input type="text" class="form-control seo-image-alt" placeholder="Alt tag"/>' +
                            '</div>' +
                            '<div class="form-group">' +
                                '<label for=""> Captions</label>' +
                                '<textarea class="form-control seo-image-captions" placeholder="Captions"></textarea>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<hr>'
        );
    }

    function autosave() {
        var data = {
            sections: getAllSectionsData(),
            seo: getAllSeoData()
        };

        $.ajax({
            url: "{{ route('write.autosave', ['theme' => $theme]) }}",
            type: 'POST',
            data: data,
            success: function () {

            },
            error: function () {
                alert('error autosaved');
            },
        });
    }

    function getAllSectionsData() {
        var count = $('.write-element').length;

        var titles = [];
        var contents = [];
        var images = [];

        $('.write-element').each(function (i, obj) {
            titles.push($(this).find('.task-heading').val());
            contents.push(tinymce.editors[i].getContent());
            images.push($(this).find('.images')[0].innerText);
        });

        let sections = [];

        for (i = 0; i < count; i++) {
            let section = {
                title: titles[i],
                images: images[i],
                content: contents[i],
                number: i + 1,
            };

            sections.push(section);
        }

        return sections;
    }

    function getAllSeoData() {
        let images = [];

        $('.images-seo-box').each(function () {
            let image = {
                name: $(this).find('.seo-image-name').val(),
                originalName: $(this).find('.seo-image-name-original').val(),
                alt: $(this).find('.seo-image-alt').val(),
                captions: $(this).find('.seo-image-captions').val(),
                order: $(this).data('order'),
            };

            images.push(image);
        });

        let data = {
            images: images
        };

        return data;
    }

    $(document).ready(function () {
        // window.onbeforeunload = function() {
        //     return "Changes may be lost";
        // }

        tinymce.init(tinymceSettings);

        attachDropzoneData();

        // Autosave svaki minut
        // window.setInterval(function () {
        //     autosave();
        // }, 5000);

        // Dodaje novi broj za pisanje
        $('#new-number').on('click', function (e) {
            var number = parseInt($('#write').children().last().data('number'));
            $('.element-box').removeClass('last');

            $('#write').append(
                '<div class="write-element last element-box" data-number="' + ++number + '">' +
                    '<div class="images" hidden></div>' +
                    '<div class="row">' +
                        '<h3 class="col font-weight-bold task-order-number">' + number + '.</h3>' +
                        '<input type="text" class="col col-md-11 task-heading form-control" placeholder="Task name">' +
                    '</div>' +
                    '<div class="form-desc"></div>' +
                    '<form action="{{ route('write.image-upload', ['theme' => $theme]) }}" class="dropzone form-group dz-clickable">' +
                        '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '">' +
                        '<div class="dz-message">' +
                            '<div>' +
                                '<h4>Drop files here or click to upload.</h4>' +
                                '<div class="text-muted">Text about explanation for slider</div>' +
                            '</div>' +
                        '</div>' +
                    '</form>' +
                    '<div class="form-group">' +
                        '<textarea name="content" id="text-editor-' + number + '" class="text-editor form-control"></textarea>' +
                    '</div>' +
                '</div>'
            );

            initDropzone($('.last').find('.dropzone'));

            tinymce.init(tinymceSettings);
        });

        @if(Route::currentRouteName() == 'write')
            $('#submit-btn').on('click', function (e) {
                var data = {
                    sections: getAllSectionsData(),
                    seo: getAllSeoData()
                };

                $.ajax({
                    url: "{{ route('write.article', ['theme' => $theme]) }}",
                    type: 'POST',
                    data: data,
                    success: function () {
                        window.location.replace("{{ route('articles.my-articles') }}");
                    },
                    error: function () {
                        alert('error autosaved');
                    },
                });
            });
        @elseif(Route::currentRouteName() == 'articles.edit-after-published')
            $('#submit-btn').on('click', function (e) {
                var data = {
                    sections: getAllSectionsData(),
                    seo: getAllSeoData()
                };

                $.ajax({
                    url: "{{ route('articles.edited-after-published', ['theme' => $theme, 'article' => $article]) }}",
                    type: 'PUT',
                    data: data,
                    success: function () {
                        window.location.replace("{{ route('articles.my-articles') }}");
                    },
                    error: function () {
                        alert('error autosaved');
                    },
                });
            });
        @elseif(Route::currentRouteName() == 'articles.edit')
            $('.edit-btn').on('click', function (e) {
                var data = {
                    sections: getAllSectionsData(),
                    seo: getAllSeoData(),
                    status: $(this).data('status')
                };

                $.ajax({
                    url: "{{ route('articles.edited', ['theme' => $article->theme, 'article' => $article]) }}",
                    type: 'PUT',
                    data: data,
                    success: function () {
                        window.location.replace("{{ route('articles.all-articles') }}");
                    },
                    error: function () {
                        alert('error autosaved');
                    },
                });
            });
        @endif

        $('.nav-link').on('click', function (e) {
            var action = $(this).data('action');

            $('.content-page[data-name != "' + action + '"]').attr('hidden', true);
            $('.content-page[data-name = "' + action + '"]').attr('hidden', false);
        });

    });

</script>
@endsection
