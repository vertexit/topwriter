@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">My Themes</h6>
            <div class="element-box">
                <h5 class="form-header">My Themes</h5>
                <div class="form-desc">
                    Your approved themes, that you can write article about right now. For new themes <a href="#">go here</a>.
                </div>
                <div class="table-responsive">
                    <table id="themes-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Got theme on</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Got theme on</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#themes-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: "/write",
            columns: [
                { data: "name" },
                { data: "category.name" },
                { data: "updated_at" },
                { data: "action" },
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

$(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.drop-theme', 'click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        var table = $("#themes-datatable").DataTable();
        var tr = $(this).parents('tr');

        if (confirm ('{{ __('Do you want to drop this theme?') }}')) {
            $.ajax({
                url: '/create/drop/' + id,
                type: 'DELETE',
                success: function (data) {
                    table.row(tr).remove().draw();
                    alert('obrisano');
                },
                error: function () {
                    alert('greska');
                }
            });
        }
    });
});

</script>

@endsection
