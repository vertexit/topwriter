@if($theme->status == 'pending')
    <button class='approve-theme btn btn-outline-success' data-id="{{ $theme->id }}">
        <i class='os-icon os-icon-edit-32'></i> Approve
    </button>
    <button class='reject-theme btn btn-outline-danger' data-id="{{ $theme->id }}">
        <i class='os-icon os-icon-edit-32'></i> Reject
    </button>
@else
    <button class='edit-theme btn btn-outline-primary' data-id="{{ $theme->id }}">
        <i class='os-icon os-icon-edit-32'></i> Edit
    </button>
@endif
