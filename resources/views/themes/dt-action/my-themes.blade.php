<a href="{{ route('write', ['theme' => $theme]) }}">
    <button class='btn btn-outline-primary'>
        <i class='os-icon os-icon-edit-32'></i> Write
    </button>
</a>
<button class='drop-theme btn btn-outline-danger' data-id="{{ $theme->id }}">
    <i class='os-icon os-icon-layout'></i> Delete
</button>
