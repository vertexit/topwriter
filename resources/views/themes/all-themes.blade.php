@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">All Themes</h6>
            <div class="element-box">
                <h5 class="form-header">All Themes</h5>
                <div class="form-desc">
                    All themes
                </div>
                <div class="table-responsive">
                    <table id="all-themes-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#all-themes-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: "/themes",
            columns: [
                { data: "name" },
                { data: "category.name" },
                { data: "status" },
                { data: "action" },

            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.approve-theme, .reject-theme', 'click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        var table = $("#all-themes-datatable").DataTable();
        var tr = $(this).parents('tr');

        if ($(this).is('.approve-theme')) {
            updateTheme('approve', id, table, tr);
        } else if ($(this).is('.reject-theme')) {
            updateTheme('reject', id, table, tr);
        }
    });
});

function updateTheme(status, id, table, tr) {
    if (confirm ('Do you want to ' + status + ' this theme?')) {
        $.ajax({
            url: '/themes/' + status + '/' + id,
            type: 'PUT',
            success: function (data) {
                table.row(tr).remove().draw();
                alert('success');
            },
            error: function () {
                alert('greska');
            }
        });
    }
}

</script>

@endsection
