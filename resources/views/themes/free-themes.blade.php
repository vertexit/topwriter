@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">Free Themes</h6>
            <div class="element-box">
                <h5 class="form-header">Free Themes</h5>
                <div class="form-desc">
                    Free themes to choose to write
                </div>
                <div class="table-responsive">
                    <table id="free-themes-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#free-themes-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: "/free-themes",
            columns: [
                { data: "name" },
                { data: "category.name" },
                { data: "action" },
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.get-free-theme', 'click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        var table = $("#free-themes-datatable").DataTable();
        var tr = $(this).parents('tr');

        if (confirm ('{{ __('Do you want to get this theme?') }}')) {
            $.ajax({
                url: '/themes/get-free/' + id,
                type: 'PUT',
                success: function (data) {
                    table.row(tr).remove().draw();
                    alert('dobili ste temu');
                },
                error: function () {
                    alert('greska');
                }
            });
        }
    });
});

</script>

@endsection
