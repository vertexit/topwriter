@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">Suggest New Theme</h6>
            <div class="element-box">
                <form action="{{ route('themes.store') }}" method="POST">
                    @csrf
                    <h5 class="form-header">Suggest New Theme</h5>
                    <div class="form-desc">
                        Suggest new theme and editor will decide if you will write it
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name"> Article name</label>
                                <input class="form-control" id="name" name="name" placeholder="Enter article name" type="text" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="category"> Category</label>
                                <select class="form-control" name="category" id="category" required>
                                    <option selected disabled value="">Select category</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="category"> About Theme</label>
                        <textarea class="form-control" name="discussion" placeholder="Start the discussion with editor" required></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-block btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="element-wrapper">
            <h6 class="element-header">Your Pending Themes</h6>
            <div class="element-box">
                <h5 class="form-header">Your Pending Themes</h5>
                <div class="form-desc">
                    Pending themes waiting for editor to approve
                </div>
                <div class="table-responsive">
                    <table id="pending-themes-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="content-panel">
        <div class="element-wrapper">
            <h6 class="element-header">Help</h6>
            <div class="element-box"></div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#pending-themes-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: "/new-theme",
            columns: [
                { data: "name" },
                { data: "category.name" },
                { data: "action" },
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.drop-theme', 'click', function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        var table = $("#pending-themes-datatable").DataTable();
        var tr = $(this).parents('tr');

        if (confirm ('{{ __('Do you want to drop this theme?') }}')) {
            $.ajax({
                url: '/themes/delete/' + id,
                type: 'DELETE',
                success: function (data) {
                    table.row(tr).remove().draw();
                    alert('odbacili ste temu');
                },
                error: function () {
                    alert('greska');
                }
            });
        }
    });
});

</script>

@endsection
