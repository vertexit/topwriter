@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">Activity Log</h6>
            <div class="element-box">
                <h5 class="form-header">Activity Log</h5>
                <div class="form-desc">
                    DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table.. <a href="https://www.datatables.net/" target="_blank">Learn More about DataTables</a>
                </div>
                <div class="table-responsive">
                    <table id="activity-log-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>Description</th>
                                <th>Subject Type</th>
                                <th>Subject</th>
                                <th>Causer Type</th>
                                <th>Causer</th>
                                <th>Properties</th>
                                <th>Created At</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Description</th>
                                <th>Subject</th>
                                <th>Causer</th>
                                <th>Properties</th>
                                <th>Created At</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#activity-log-datatable").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax: "/activity-log", 
            columns: [
                { data: "description" },
                { data: "subject_type" },
                { data: "subject_id" },
                { data: "causer_type" },
                { data: "causer_id" },
                { data: "properties" },
                { data: "created_at" },
            ],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});

</script>

@endsection
