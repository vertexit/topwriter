@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">All Roles</h6>
            <div class="element-box">
                <h5 class="form-header">All Roles</h5>
                <div class="table-responsive">
                    <table id="roles-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Permissions</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Permissions</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#roles-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: "/roles",
            columns: [
                { data: "id" },
                { data: "name" },
                {
                    data: "permissions",
                    searchable: false,
                    orderable: false
                },
                { data: "action" }
            ],
            columnDefs: [
                {
                    targets: 2,
                    render: function (data) {
                        roles = [];
                        $.each(data, function (i, role) {
                            roles.push(permission.name);
                        });

                        return roles.join(', ');
                    }
                }
            ]
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});

</script>

@endsection
