@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">All Users</h6>
            <div class="element-box">
                <h5 class="form-header">All Users</h5>
                <div class="table-responsive">
                    <table id="users-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Number of articles</th>
                                <th>Number of themes</th>
                                <th>Roles</th>
                                <th>Special Permissions</th>
                                <th>Registered at</th>
                                <th>Last active</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Username</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Number of articles</th>
                                <th>Number of themes</th>
                                <th>Roles</th>
                                <th>Special Permissions</th>
                                <th>Registered at</th>
                                <th>Last active</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#users-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: "/users",
            columns: [
                { data: "username" },
                { data: "first_name" },
                { data: "last_name" },
                { data: "email" },
                {
                    data: "articles_count",
                    searchable: false,
                    orderable: false
                },
                {
                    data: "themes_count",
                    searchable: false,
                    orderable: false
                },
                {
                    data: "roles",
                    searchable: false,
                    orderable: false
                },
                {
                    data: "permissions",
                    searchable: false,
                    orderable: false
                },
                { data: "created_at" },
                { data: "created_at" },
                { data: "action" },
            ],
            columnDefs: [
                {
                    targets: 0,
                    render: function (data, type, row, meta) {
                        return "<a href='#'>" + data + "</a>";
                    }
                },
                {
                    targets: 7,
                    render: function (data) {
                        permissions = [];
                        $.each(data, function (i, permission) {
                            permissions.push(permission.name);
                        });

                        return permissions.join(', ');
                    }
                },
                {
                    targets: 6,
                    // title: "Roles",
                    render: function (data) {
                        roles = [];
                        $.each(data, function (i, role) {
                            roles.push(role.name);
                        });

                        return roles.join(', ');
                    }
            }]
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});

</script>

@endsection
