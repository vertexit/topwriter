<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">

    <link href="{{ asset('favicon.png') }}" rel="shortcut icon">
    {{-- <link href="apple-touch-icon.png" rel="apple-touch-icon"> --}}
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
    {{-- <link href="bower_components/select2/dist/css/select2.min.css" rel="stylesheet">
    <link href="bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="bower_components/dropzone/dist/dropzone.css" rel="stylesheet">
    <link href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet">
    <link href="bower_components/slick-carousel/slick/slick.css" rel="stylesheet"> --}}
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    @yield('external-css')
    {{-- <link rel="stylesheet" href="{{ asset('js/dataTables.bootstrap.min.css') }}"> --}}
    <title>Top Writer</title>
</head>
<body class="menu-position-side menu-side-left full-screen">

    <div class="all-wrapper no-padding-content solid-bg-all">
        {{-- @include('components/popover') --}}

            <div class="layout-w">
                @include('components/mobile-header')
                @include('components/sidebar')
                @include('components/floating-options') 
                
                <div class="content-w">
                    @include('components/header')
                    @include('components/breadcrumbs')
                    
                    @yield('content')
                </div>
            </div>

    </div>
    
    <script src="{{ asset('js/jquery.min.js') }}"></script>

    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/Chart.min.js') }}"></script>
    <script src="{{ asset('js/select2.full.min.js') }}"></script>

    <script src="{{ asset('js/daterangepicker.css') }}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>

    <script src="{{ asset('js/util.js') }}"></script>
    <script src="{{ asset('js/alert.js') }}"></script>
    <script src="{{ asset('js/button.js') }}"></script>
    <script src="{{ asset('js/carousel.js') }}"></script>
    <script src="{{ asset('js/collapse.js') }}"></script>
    <script src="{{ asset('js/dropdown.js') }}"></script>
    <script src="{{ asset('js/modal.js') }}"></script>
    <script src="{{ asset('js/tab.js') }}"></script>
    <script src="{{ asset('js/tooltip.js') }}"></script>
    <script src="{{ asset('js/popover.js') }}"></script>

    <script src="{{ asset('js/dropzone.js') }}"></script>

    <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/demo_customizer.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    
    @yield('external-js')
</body>
</html>
