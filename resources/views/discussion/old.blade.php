@extends('layouts.discussion')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="app-email-w">
            <div class="app-email-i">
                <!--------------------
                    START - Email Side menu
                    -------------------->
                <div class="ae-side-menu">
                    <div class="aem-head">
                        <a class="ae-side-menu-toggler" href="#"><i class="os-icon os-icon-hamburger-menu-2"></i></a>
                    </div>
                    <ul class="ae-main-menu">
                        <li class="active">
                            <a href="#"><i class="os-icon os-icon-phone-21"></i><span>Inbox</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="os-icon os-icon-ui-92"></i><span>Sent</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="os-icon os-icon-documents-03"></i><span>Draft</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="os-icon os-icon-ui-15"></i><span>Trash</span></a>
                        </li>
                        <li>
                            <a href="#"><i class="os-icon os-icon-ui-44"></i><span>Archive</span></a>
                        </li>
                    </ul>
                    <div class="ae-labels">
                        <div class="ae-labels-header">
                            <i class="os-icon os-icon-ui-54"></i><span>Labels</span>
                        </div>
                        <a class="ae-label ae-label-red" href="#"><span class="label-pin"></span><span class="label-value">Corporate</span></a><a class="ae-label ae-label-green" href="#"><span class="label-pin"></span><span class="label-value">Personal</span></a><a class="ae-label ae-label-blue" href="#"><span class="label-pin"></span><span class="label-value">Statements</span></a><a class="ae-label ae-label-yellow" href="#"><span class="label-pin"></span><span class="label-value">Projects</span></a>
                    </div>
                </div>
                <!--------------------
                    END - Email Side menu
                    --------------------><!--------------------
                    START - Email Messages List
                    -------------------->
                <div class="ae-list-w">
                    <div class="ael-head">
                        <div class="actions-left">
                            <select class="form-control">
                                <option class="">
                                    Sort by status
                                </option>
                                <option class="">
                                    Sort by date
                                </option>
                            </select>
                        </div>
                        <div class="actions-right">
                            <a href="#"><i class="os-icon os-icon-ui-37"></i></a><a href="#"><i class="os-icon os-icon-grid-18"></i></a>
                        </div>
                    </div>
                    <div class="ae-list ps ps--theme_default ps--active-y" data-ps-id="22a3faf9-b35d-e8be-daf2-7351b7f9cf78">
                        <div class="ae-item with-status  status-green">
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    1:25pm
                                </div>
                                <h6 class="aei-title">
                                    Top 10 reasons that Germany lost the world cup in Russia last week online
                                </h6>
                                <div class="aei-sub-title">
                                    Pending
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    1:25pm
                                </div>
                                <h6 class="aei-title">
                                    Top 10 reasons that Germany lost the world cup in Russia last week online
                                </h6>
                                <div class="aei-sub-title">
                                    Pending
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    1:25pm
                                </div>
                                <h6 class="aei-title">
                                    Top 10 reasons that Germany lost the world cup in Russia last week online
                                </h6>
                                <div class="aei-sub-title">
                                    Pending
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    1:25pm
                                </div>
                                <h6 class="aei-title">
                                    Top 10 reasons that Germany lost the world cup in Russia last week online
                                </h6>
                                <div class="aei-sub-title">
                                    Pending
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    1:25pm
                                </div>
                                <h6 class="aei-title">
                                    Top 10 reasons that Germany lost the world cup in Russia last week online
                                </h6>
                                <div class="aei-sub-title">
                                    Pending
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar2.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    11:12am
                                </div>
                                <h6 class="aei-title">
                                    Kyle Jefferson
                                </h6>
                                <div class="aei-sub-title">
                                    Document Verification
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status active status-red">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar3.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    9:07am
                                </div>
                                <h6 class="aei-title">
                                    Matt Wallas
                                </h6>
                                <div class="aei-sub-title">
                                    Booking Confirmation
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar4.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    6:34am
                                </div>
                                <h6 class="aei-title">
                                    Kimerley Markson
                                </h6>
                                <div class="aei-sub-title">
                                    Your Statement is available
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-blue">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar5.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    Yesterday
                                </div>
                                <h6 class="aei-title">
                                    Lora Miller
                                </h6>
                                <div class="aei-sub-title">
                                    New comment on your blog post
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-blue">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar6.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    Yesterday
                                </div>
                                <h6 class="aei-title">
                                    Ivan Moskovits
                                </h6>
                                <div class="aei-sub-title">
                                    Diagnostics validation
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar7.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    Yesterday
                                </div>
                                <h6 class="aei-title">
                                    Sheldon Kooper
                                </h6>
                                <div class="aei-sub-title">
                                    Your trial has ended
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-yellow">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar1.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    2 days ago
                                </div>
                                <h6 class="aei-title">
                                    Mesut Ozil
                                </h6>
                                <div class="aei-sub-title">
                                    New comment received
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-red">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar4.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    3 days ago
                                </div>
                                <h6 class="aei-title">
                                    Andres Iniesta
                                </h6>
                                <div class="aei-sub-title">
                                    Certificate is issued
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-blue">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar5.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    Yesterday
                                </div>
                                <h6 class="aei-title">
                                    Lora Miller
                                </h6>
                                <div class="aei-sub-title">
                                    New comment on your blog post
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-blue">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar6.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    Yesterday
                                </div>
                                <h6 class="aei-title">
                                    Ivan Moskovits
                                </h6>
                                <div class="aei-sub-title">
                                    Diagnostics validation
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar7.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    Yesterday
                                </div>
                                <h6 class="aei-title">
                                    Sheldon Kooper
                                </h6>
                                <div class="aei-sub-title">
                                    Your trial has ended
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-yellow">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar1.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    2 days ago
                                </div>
                                <h6 class="aei-title">
                                    Mesut Ozil
                                </h6>
                                <div class="aei-sub-title">
                                    New comment received
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ae-item with-status  status-green">
                            <div class="aei-image">
                                <div class="user-avatar-w">
                                    <img alt="" src="img/avatar2.jpg">
                                </div>
                            </div>
                            <div class="aei-content">
                                <div class="aei-timestamp">
                                    1 week ago
                                </div>
                                <h6 class="aei-title">
                                    Anthony Tailor
                                </h6>
                                <div class="aei-sub-title">
                                    Create new profile request
                                </div>
                                <div class="aei-text">
                                    When the equation, first to ability the forwards, the a but travelling
                                </div>
                            </div>
                        </div>
                        <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                            <div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                        </div>
                        <div class="ps__scrollbar-y-rail" style="top: 0px; height: 1000px; right: 0px;">
                            <div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 600px;"></div>
                        </div>
                    </div>
                    <a class="ae-load-more" href="#"><span>Load More Messages</span></a>
                </div>
                <!--------------------
                    END - Email Messages List
                    -------------------->
                <div class="ae-content-w">
                    <!--------------------
                        START - Email Content Header
                        -------------------->
                    <div class="aec-head">
                        <div class="actions-left">
                            <a class="highlight" href="#"><i class="os-icon os-icon-ui-02"></i></a>
                        </div>
                        <div class="actions-right">
                            <div class="aeh-actions">
                                <a href="#"><i class="os-icon os-icon-ui-44"></i></a><a class="separate" href="#"><i class="os-icon os-icon-ui-15"></i></a><a href="#"><i class="os-icon os-icon-common-07"></i></a><a href="#"><i class="os-icon os-icon-mail-19"></i></a>
                            </div>
                            <div class="user-avatar">
                                <img alt="" src="img/avatar3.jpg">
                            </div>
                        </div>
                    </div>
                    <!--------------------
                        END - Email Content Header
                        --------------------><!--------------------
                        START - Email Content
                        -------------------->
                    <div class="ae-content">
                        <div class="older-pack">
                            <div class="aec-full-message-w">
                                <div class="aec-full-message">
                                    <div class="message-head">
                                        <div class="user-w with-status status-green">
                                            <div class="user-avatar-w">
                                                <div class="user-avatar">
                                                    <img alt="" src="img/avatar1.jpg">
                                                </div>
                                            </div>
                                            <div class="user-name">
                                                <h6 class="user-title">
                                                    User
                                                </h6>
                                                <div class="user-role">
                                                    Writer<span>&lt; some.email@email.com &gt;</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="message-info">
                                            February 7th, 2017<br>2:27pm
                                        </div>
                                    </div>
                                    <div class="message-content">
                                        What I want to write about<br>
                                        How well I know the matter<br>
                                        In what tone will it be written<br>
                                        Who is it intended for<br>
                                        <div class="message-attachments">
                                            <div class="attachments-heading">
                                                Attachments
                                            </div>
                                            <div class="attachments-docs">
                                                <a href="#"><i class="os-icon os-icon-documents-07"></i><span>Image File</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="aec-full-message-w show-pack">
                            <div class="more-messages">
                                7 Earlier Messages
                            </div>
                            <div class="aec-full-message">
                                <div class="message-head">
                                    <div class="user-w with-status status-green">
                                        <div class="user-avatar-w">
                                            <div class="user-avatar">
                                                <img alt="" src="img/avatar1.jpg">
                                            </div>
                                        </div>
                                        <div class="user-name">
                                            <h6 class="user-title">
                                                John Mayers
                                            </h6>
                                            <div class="user-role">
                                                Account Manager<span>&lt; john@solutions.com &gt;</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message-info">
                                        January 12th, 2017<br>1:24pm
                                    </div>
                                </div>
                                <div class="message-content">
                                    Hi Mike,<br><br>When the equation, first to ability the forwards, the a but travelling, outlines sentinels bad expand to goodness. Behind if have at the even I and how work, completely deference who boss actually designer; Monstrous with geared from far and these, morals, phase rome; Class. Called get amidst of next.<br><br>Regards,<br>Mike Mayers
                                    <div class="message-attachments">
                                        <div class="attachments-heading">
                                            Attachments
                                        </div>
                                        <div class="attachments-docs">
                                            <a href="#"><i class="os-icon os-icon-ui-51"></i><span>Excel Document</span></a><a href="#"><i class="os-icon os-icon-documents-07"></i><span>Image File</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="aec-reply">
                            <div class="reply-header">
                                <h5>
                                    Reply to <span>John Mayers</span>
                                </h5>
                            </div>
                            <textarea cols="80" id="ckeditorEmail" name="ckeditor1" rows="5" style="visibility: hidden; display: none;"></textarea>
                            <div id="cke_ckeditorEmail" class="cke_1 cke cke_reset cke_chrome cke_editor_ckeditorEmail cke_ltr cke_browser_webkit" dir="ltr" lang="en" role="application" aria-labelledby="cke_ckeditorEmail_arialbl">
                                <span id="cke_ckeditorEmail_arialbl" class="cke_voice_label">Rich Text Editor, ckeditorEmail</span>
                                <div class="cke_inner cke_reset" role="presentation">
                                    <span id="cke_1_top" class="cke_top cke_reset_all" role="presentation" style="height: auto; user-select: none;"><span id="cke_8" class="cke_voice_label">Editor toolbars</span><span id="cke_1_toolbox" class="cke_toolbox" role="group" aria-labelledby="cke_8" onmousedown="return false;"><span id="cke_9" class="cke_toolbar cke_toolbar_last" role="toolbar"><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><a id="cke_10" class="cke_button cke_button__bold cke_button_off" href="javascript:void('Bold')" title="Bold (Ctrl+B)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_10_label" aria-describedby="cke_10_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(2,event);" onfocus="return CKEDITOR.tools.callFunction(3,event);" onclick="CKEDITOR.tools.callFunction(4,this);return false;"><span class="cke_button_icon cke_button__bold_icon" style="background-image:url('file:///home/mile/Downloads/themeforest-19760124-light-admin-crypto-clean-bootstrap-4-dashboard-admin-html-template/light-admin-all-files/html_admin/dist/bower_components/ckeditor/plugins/icons.png?t=HBDF');background-position:0 -24px;background-size:auto;">&nbsp;</span><span id="cke_10_label" class="cke_button_label cke_button__bold_label" aria-hidden="false">Bold</span><span id="cke_10_description" class="cke_button_label" aria-hidden="false">Keyboard shortcut Ctrl+B</span></a><a id="cke_11" class="cke_button cke_button__italic cke_button_off" href="javascript:void('Italic')" title="Italic (Ctrl+I)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_11_label" aria-describedby="cke_11_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(5,event);" onfocus="return CKEDITOR.tools.callFunction(6,event);" onclick="CKEDITOR.tools.callFunction(7,this);return false;"><span class="cke_button_icon cke_button__italic_icon" style="background-image:url('file:///home/mile/Downloads/themeforest-19760124-light-admin-crypto-clean-bootstrap-4-dashboard-admin-html-template/light-admin-all-files/html_admin/dist/bower_components/ckeditor/plugins/icons.png?t=HBDF');background-position:0 -48px;background-size:auto;">&nbsp;</span><span id="cke_11_label" class="cke_button_label cke_button__italic_label" aria-hidden="false">Italic</span><span id="cke_11_description" class="cke_button_label" aria-hidden="false">Keyboard shortcut Ctrl+I</span></a><span class="cke_toolbar_separator" role="separator"></span><a id="cke_12" class="cke_button cke_button__numberedlist cke_button_off" href="javascript:void('Insert/Remove Numbered List')" title="Insert/Remove Numbered List" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_12_label" aria-describedby="cke_12_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(8,event);" onfocus="return CKEDITOR.tools.callFunction(9,event);" onclick="CKEDITOR.tools.callFunction(10,this);return false;"><span class="cke_button_icon cke_button__numberedlist_icon" style="background-image:url('file:///home/mile/Downloads/themeforest-19760124-light-admin-crypto-clean-bootstrap-4-dashboard-admin-html-template/light-admin-all-files/html_admin/dist/bower_components/ckeditor/plugins/icons.png?t=HBDF');background-position:0 -1440px;background-size:auto;">&nbsp;</span><span id="cke_12_label" class="cke_button_label cke_button__numberedlist_label" aria-hidden="false">Insert/Remove Numbered List</span><span id="cke_12_description" class="cke_button_label" aria-hidden="false"></span></a><a id="cke_13" class="cke_button cke_button__bulletedlist cke_button_off" href="javascript:void('Insert/Remove Bulleted List')" title="Insert/Remove Bulleted List" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_13_label" aria-describedby="cke_13_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(11,event);" onfocus="return CKEDITOR.tools.callFunction(12,event);" onclick="CKEDITOR.tools.callFunction(13,this);return false;"><span class="cke_button_icon cke_button__bulletedlist_icon" style="background-image:url('file:///home/mile/Downloads/themeforest-19760124-light-admin-crypto-clean-bootstrap-4-dashboard-admin-html-template/light-admin-all-files/html_admin/dist/bower_components/ckeditor/plugins/icons.png?t=HBDF');background-position:0 -1392px;background-size:auto;">&nbsp;</span><span id="cke_13_label" class="cke_button_label cke_button__bulletedlist_label" aria-hidden="false">Insert/Remove Bulleted List</span><span id="cke_13_description" class="cke_button_label" aria-hidden="false"></span></a><span class="cke_toolbar_separator" role="separator"></span><a id="cke_14" class="cke_button cke_button__link cke_button_off" href="javascript:void('Link')" title="Link (Ctrl+L)" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_14_label" aria-describedby="cke_14_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(14,event);" onfocus="return CKEDITOR.tools.callFunction(15,event);" onclick="CKEDITOR.tools.callFunction(16,this);return false;"><span class="cke_button_icon cke_button__link_icon" style="background-image:url('file:///home/mile/Downloads/themeforest-19760124-light-admin-crypto-clean-bootstrap-4-dashboard-admin-html-template/light-admin-all-files/html_admin/dist/bower_components/ckeditor/plugins/icons.png?t=HBDF');background-position:0 -1320px;background-size:auto;">&nbsp;</span><span id="cke_14_label" class="cke_button_label cke_button__link_label" aria-hidden="false">Link</span><span id="cke_14_description" class="cke_button_label" aria-hidden="false">Keyboard shortcut Ctrl+L</span></a><a id="cke_15" class="cke_button cke_button__unlink cke_button_disabled " href="javascript:void('Unlink')" title="Unlink" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_15_label" aria-describedby="cke_15_description" aria-haspopup="false" aria-disabled="true" onkeydown="return CKEDITOR.tools.callFunction(17,event);" onfocus="return CKEDITOR.tools.callFunction(18,event);" onclick="CKEDITOR.tools.callFunction(19,this);return false;"><span class="cke_button_icon cke_button__unlink_icon" style="background-image:url('file:///home/mile/Downloads/themeforest-19760124-light-admin-crypto-clean-bootstrap-4-dashboard-admin-html-template/light-admin-all-files/html_admin/dist/bower_components/ckeditor/plugins/icons.png?t=HBDF');background-position:0 -1344px;background-size:auto;">&nbsp;</span><span id="cke_15_label" class="cke_button_label cke_button__unlink_label" aria-hidden="false">Unlink</span><span id="cke_15_description" class="cke_button_label" aria-hidden="false"></span></a><span class="cke_toolbar_separator" role="separator"></span><a id="cke_16" class="cke_button cke_button__about cke_button_off" href="javascript:void('About CKEditor 4')" title="About CKEditor 4" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_16_label" aria-describedby="cke_16_description" aria-haspopup="false" onkeydown="return CKEDITOR.tools.callFunction(20,event);" onfocus="return CKEDITOR.tools.callFunction(21,event);" onclick="CKEDITOR.tools.callFunction(22,this);return false;"><span class="cke_button_icon cke_button__about_icon" style="background-image:url('file:///home/mile/Downloads/themeforest-19760124-light-admin-crypto-clean-bootstrap-4-dashboard-admin-html-template/light-admin-all-files/html_admin/dist/bower_components/ckeditor/plugins/icons.png?t=HBDF');background-position:0 0px;background-size:auto;">&nbsp;</span><span id="cke_16_label" class="cke_button_label cke_button__about_label" aria-hidden="false">About CKEditor 4</span><span id="cke_16_description" class="cke_button_label" aria-hidden="false"></span></a></span><span class="cke_toolbar_end"></span></span></span></span>
                                    <div id="cke_1_contents" class="cke_contents cke_reset" role="presentation" style="height: 110px;"><span id="cke_20" class="cke_voice_label">Press ALT 0 for help</span><iframe src="" frameborder="0" class="cke_wysiwyg_frame cke_reset" style="width: 100%; height: 100%;" title="Rich Text Editor, ckeditorEmail" aria-describedby="cke_20" tabindex="0" allowtransparency="true"></iframe></div>
                                    <span id="cke_1_bottom" class="cke_bottom cke_reset_all" role="presentation" style="user-select: none;"><span id="cke_1_resizer" class="cke_resizer cke_resizer_vertical cke_resizer_ltr" title="Resize" onmousedown="CKEDITOR.tools.callFunction(1, event)">◢</span><span id="cke_1_path_label" class="cke_voice_label">Elements path</span><span id="cke_1_path" class="cke_path" role="group" aria-labelledby="cke_1_path_label"><span class="cke_path_empty">&nbsp;</span></span></span>
                                </div>
                            </div>
                            <div class="buttons-w">
                                <div class="actions-left">
                                    <a class="btn btn-link" href="#"><i class="os-icon os-icon-ui-51"></i><span>Add Attachment</span></a>
                                </div>
                                <div class="actions-right">
                                    <a class="btn btn-success" href="#"><i class="os-icon os-icon-mail-18"></i><span>Send Message</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--------------------
                        END - Email Content
                        -------------------->
                </div>
            </div>
        </div>
        <!--------------------
            START - Color Scheme Toggler
            -------------------->
        <div class="floated-colors-btn second-floated-btn">
            <div class="os-toggler-w">
                <div class="os-toggler-i">
                    <div class="os-toggler-pill"></div>
                </div>
            </div>
            <span>Dark </span><span>Colors</span>
        </div>
        <!--------------------
            END - Color Scheme Toggler
            --------------------><!--------------------
            START - Demo Customizer
            -------------------->
        <div class="floated-customizer-btn third-floated-btn">
            <div class="icon-w">
                <i class="os-icon os-icon-ui-46"></i>
            </div>
            <span>Customizer</span>
        </div>
        <div class="floated-customizer-panel">
            <div class="fcp-content">
                <div class="close-customizer-btn">
                    <i class="os-icon os-icon-x"></i>
                </div>
                <div class="fcp-group">
                    <div class="fcp-group-header">
                        Menu Settings
                    </div>
                    <div class="fcp-group-contents">
                        <div class="fcp-field">
                            <label for="">Menu Position</label>
                            <select class="menu-position-selector">
                                <option value="left">
                                    Left
                                </option>
                                <option value="right">
                                    Right
                                </option>
                                <option value="top">
                                    Top
                                </option>
                            </select>
                        </div>
                        <div class="fcp-field">
                            <label for="">Menu Style</label>
                            <select class="menu-layout-selector">
                                <option value="compact">
                                    Compact
                                </option>
                                <option value="full">
                                    Full
                                </option>
                                <option value="mini">
                                    Mini
                                </option>
                            </select>
                        </div>
                        <div class="fcp-field with-image-selector-w" style="display: none;">
                            <label for="">With Image</label>
                            <select class="with-image-selector">
                                <option value="yes">
                                    Yes
                                </option>
                                <option value="no">
                                    No
                                </option>
                            </select>
                        </div>
                        <div class="fcp-field">
                            <label for="">Menu Color</label>
                            <div class="fcp-colors menu-color-selector">
                                <div class="color-selector menu-color-selector color-bright selected"></div>
                                <div class="color-selector menu-color-selector color-dark"></div>
                                <div class="color-selector menu-color-selector color-light"></div>
                                <div class="color-selector menu-color-selector color-transparent"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fcp-group">
                    <div class="fcp-group-header">
                        Sub Menu
                    </div>
                    <div class="fcp-group-contents">
                        <div class="fcp-field">
                            <label for="">Sub Menu Style</label>
                            <select class="sub-menu-style-selector">
                                <option value="flyout">
                                    Flyout
                                </option>
                                <option value="inside">
                                    Inside/Click
                                </option>
                                <option value="over">
                                    Over
                                </option>
                            </select>
                        </div>
                        <div class="fcp-field">
                            <label for="">Sub Menu Color</label>
                            <div class="fcp-colors">
                                <div class="color-selector sub-menu-color-selector color-bright selected"></div>
                                <div class="color-selector sub-menu-color-selector color-dark"></div>
                                <div class="color-selector sub-menu-color-selector color-light"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fcp-group">
                    <div class="fcp-group-header">
                        Other Settings
                    </div>
                    <div class="fcp-group-contents">
                        <div class="fcp-field">
                            <label for="">Full Screen?</label>
                            <select class="full-screen-selector">
                                <option value="yes">
                                    Yes
                                </option>
                                <option value="no">
                                    No
                                </option>
                            </select>
                        </div>
                        <div class="fcp-field">
                            <label for="">Show Top Bar</label>
                            <select class="top-bar-visibility-selector">
                                <option value="yes">
                                    Yes
                                </option>
                                <option value="no">
                                    No
                                </option>
                            </select>
                        </div>
                        <div class="fcp-field">
                            <label for="">Above Menu?</label>
                            <select class="top-bar-above-menu-selector">
                                <option value="yes">
                                    Yes
                                </option>
                                <option value="no">
                                    No
                                </option>
                            </select>
                        </div>
                        <div class="fcp-field">
                            <label for="">Top Bar Color</label>
                            <div class="fcp-colors">
                                <div class="color-selector top-bar-color-selector color-bright"></div>
                                <div class="color-selector top-bar-color-selector color-dark"></div>
                                <div class="color-selector top-bar-color-selector color-light"></div>
                                <div class="color-selector top-bar-color-selector color-transparent selected"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--------------------
            END - Demo Customizer
            --------------------><!--------------------
            START - Chat Popup Box
            -------------------->
        <div class="floated-chat-btn">
            <i class="os-icon os-icon-mail-07"></i><span>Demo Chat</span>
        </div>
        <div class="floated-chat-w">
            <div class="floated-chat-i">
                <div class="chat-close">
                    <i class="os-icon os-icon-close"></i>
                </div>
                <div class="chat-head">
                    <div class="user-w with-status status-green">
                        <div class="user-avatar-w">
                            <div class="user-avatar">
                                <img alt="" src="img/avatar1.jpg">
                            </div>
                        </div>
                        <div class="user-name">
                            <h6 class="user-title">
                                John Mayers
                            </h6>
                            <div class="user-role">
                                Account Manager
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chat-messages ps ps--theme_default" data-ps-id="e4ef4ca6-ad87-c3ac-edc4-2173440df8c6">
                    <div class="message">
                        <div class="message-content">
                            Hi, how can I help you?
                        </div>
                    </div>
                    <div class="date-break">
                        Mon 10:20am
                    </div>
                    <div class="message">
                        <div class="message-content">
                            Hi, my name is Mike, I will be happy to assist you
                        </div>
                    </div>
                    <div class="message self">
                        <div class="message-content">
                            Hi, I tried ordering this product and it keeps showing me error code.
                        </div>
                    </div>
                    <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                        <div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;">
                        <div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
                    </div>
                </div>
                <div class="chat-controls">
                    <input class="message-input" placeholder="Type your message here..." type="text">
                    <div class="chat-extra">
                        <a href="#"><span class="extra-tooltip">Attach Document</span><i class="os-icon os-icon-documents-07"></i></a><a href="#"><span class="extra-tooltip">Insert Photo</span><i class="os-icon os-icon-others-29"></i></a><a href="#"><span class="extra-tooltip">Upload Video</span><i class="os-icon os-icon-ui-51"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!--------------------
            END - Chat Popup Box
            -------------------->
    </div>
</div>

@endsection
