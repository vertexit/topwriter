<div class="menu-w selected-menu-color-light menu-activated-on-hover menu-has-selected-link color-scheme-dark color-style-bright sub-menu-color-bright menu-position-side menu-side-left menu-layout-compact sub-menu-style-over">
    <div class="logo-w">
        <a class="logo" href="{{ route('dashboard') }}">
            <div class="logo-element"></div>
            <div class="logo-label">Top Writer</div>
        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w">
                <img alt="" src="{{ asset('img/avatar1.jpg') }}">
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-name">
                    {{ Auth::user()->username }}
                </div>
                <div class="logged-user-role">
                    {{ Auth::user()->mainRole }}
                </div>
            </div>
            <div class="logged-user-toggler-arrow">
                <div class="os-icon os-icon-chevron-down"></div>
            </div>
            <div class="logged-user-menu color-style-bright">
                <div class="logged-user-avatar-info">
                    <div class="avatar-w">
                        <img alt="" src="{{ asset('img/avatar1.jpg') }}">
                    </div>
                    <div class="logged-user-info-w">
                        <div class="logged-user-name">
                            {{ Auth::user()->username }}
                        </div>
                        <div class="logged-user-role">
                            {{ Auth::user()->mainRole }}
                        </div>
                    </div>
                </div>
                <div class="bg-icon">
                    <i class="os-icon os-icon-wallet-loaded"></i>
                </div>
                <ul>
                    <li>
                        <a href="#">
                            <i class="os-icon os-icon-mail-01"></i>
                            <span>Incoming Mail</span>
                        </a>
                    </li>
                    <li>
                        <a href="users_profile_big.html"><i class="os-icon os-icon-user-male-circle2"></i><span>Profile Details</span></a>
                    </li>
                    <li>
                        <a href="users_profile_small.html"><i class="os-icon os-icon-coins-4"></i><span>Billing Details</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="os-icon os-icon-others-43"></i><span>Notifications</span></a>
                    </li>
                    <li>
                        <a href="#"><i class="os-icon os-icon-signs-11"></i><span>Logout</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="menu-actions">
<!--------------------
START - Messages Link in secondary top menu
-------------------->
<div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
    <i class="os-icon os-icon-mail-14"></i>
    <div class="new-messages-count">12</div>
    <div class="os-dropdown light message-list">
        <ul>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="{{ asset('img/avatar1.jpg') }}">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            John Mayers
                        </h6>
                        <h6 class="message-title">
                            Account Update
                        </h6>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="img/avatar2.jpg">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            Phil Jones
                        </h6>
                        <h6 class="message-title">
                            Secutiry Updates
                        </h6>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="img/avatar3.jpg">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            Bekky Simpson
                        </h6>
                        <h6 class="message-title">
                            Vacation Rentals
                        </h6>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="img/avatar4.jpg">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            Alice Priskon
                        </h6>
                        <h6 class="message-title">
                            Payment Confirmation
                        </h6>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>
<!--------------------
END - Messages Link in secondary top menu
--------------------><!--------------------
START - Settings Link in secondary top menu
-------------------->
<div class="top-icon top-settings os-dropdown-trigger os-dropdown-position-right">
    <i class="os-icon os-icon-ui-46"></i>
    <div class="os-dropdown">
        <div class="icon-w">
            <i class="os-icon os-icon-ui-46"></i>
        </div>
        <ul>
            <li>
                <a href="users_profile_small.html">
                    <i class="os-icon os-icon-ui-49"></i>
                    <span>Profile Settings</span>
                </a>
            </li>
            <li>
                <a href="users_profile_small.html"><i class="os-icon os-icon-grid-10"></i><span>Billing Info</span></a>
            </li>
            <li>
                <a href="users_profile_small.html"><i class="os-icon os-icon-ui-44"></i><span>My Invoices</span></a>
            </li>
            <li>
                <a href="users_profile_small.html"><i class="os-icon os-icon-ui-15"></i><span>Cancel Account</span></a>
            </li>
        </ul>
    </div>
</div>
<!--------------------
END - Settings Link in secondary top menu
--------------------><!--------------------
START - Messages Link in secondary top menu
-------------------->
<div class="messages-notifications os-dropdown-trigger os-dropdown-position-right">
    <i class="os-icon os-icon-zap"></i>
    <div class="new-messages-count">
        4
    </div>
    <div class="os-dropdown light message-list">
        <div class="icon-w">
            <i class="os-icon os-icon-zap"></i>
        </div>
        <ul>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="img/avatar1.jpg">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            John Mayers
                        </h6>
                        <h6 class="message-title">
                            Account Update
                        </h6>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="img/avatar2.jpg">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            Phil Jones
                        </h6>
                        <h6 class="message-title">
                            Secutiry Updates
                        </h6>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="img/avatar3.jpg">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            Bekky Simpson
                        </h6>
                        <h6 class="message-title">
                            Vacation Rentals
                        </h6>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <div class="user-avatar-w">
                        <img alt="" src="img/avatar4.jpg">
                    </div>
                    <div class="message-content">
                        <h6 class="message-from">
                            Alice Priskon
                        </h6>
                        <h6 class="message-title">
                            Payment Confirmation
                        </h6>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div>
<!--------------------
END - Messages Link in secondary top menu
-------------------->
</div>
<div class="element-search autosuggest-search-activator">
    <input placeholder="Start typing to search..." type="text">
</div>
<h1 class="menu-page-header">Page Header</h1>
<ul class="main-menu">
    @role('super admin')
        <li class="sub-header">
            <span>Admin</span>
        </li>
        @can('users view')
            <li class="selected has-sub-menu">
                <a href="{{ route('users.index') }}">
                    <div class="icon-w">
                        <div class="os-icon os-icon-layout"></div>
                    </div>
                    <span>Users</span>
                </a>
                <div class="sub-menu-w">
                    <div class="sub-menu-header">Users</div>
                    <div class="sub-menu-icon">
                        <i class="os-icon os-icon-layout"></i>
                    </div>
                    <div class="sub-menu-i">
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ route('users.index') }}">All users</a>
                            </li>
                            <li>
                                <a href="{{ route('roles.index') }}">Roles</a>
                            </li>
                            <li>
                                <a href="{{ route('permissions.index') }}">Permissions</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        @endcan
        <li>
            <a href="{{ route('activity-log') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div>
                <span>Activity Log</span>
            </a>
        </li>
        <li>
            <a href="#">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div>
                <span>Advanced Analytics</span>
            </a>
        </li>
    @endrole
    @role('editor')
        <li class="sub-header">
            <span>Editor</span>
        </li>
        <li>
            <a href="{{ route('articles.all-articles') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div>
                <span>Articles</span>
            </a>
        </li>
        <li>
            <a href="{{ route('themes.all-themes') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div>
                <span>Themes</span>
            </a>
        </li>
        <li>
            <a href="{{ route('categories.all-categories') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Categories</span>
            </a>
        </li>
    @endrole
    <li class="sub-header">
        <span>Themes</span>
    </li>
    @can('discussion read')
        <li>
            <a href="{{ route('discussion.index') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-edit-32"></div>
                </div>
                <span>Discussion</span>
            </a>
        </li>
    @endcan
    @can('theme create')
        <li>
            <a href="{{ route('themes.new') }}">
                <div class="icon-w">
                    <div class="os-icon os-icon-edit-32"></div>
                </div>
                <span>Suggest New Theme</span>
            </a>
        </li>
    @endcan
    <li>
        <a href="{{ route('themes.free') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-edit-32"></div>
            </div>
            <span>Free Themes</span>
        </a>
    </li>
    <li class="sub-header">
        <span>Articles</span>
    </li>
    <li class="selected has-sub-menu">
        <a href="index.html">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Write</span>
        </a>
        <div class="sub-menu-w">
            <div class="sub-menu-header">Write</div>
            <div class="sub-menu-icon">
                <i class="os-icon os-icon-layout"></i>
            </div>
            <div class="sub-menu-i">
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('themes.create') }}">New Article</a>
                    </li>
                    <li>
                        <a href="#">My Articles</a>
                    </li>
                </ul>
            </div>
        </div>
    </li>
    <li>
        <a href="{{ route('themes.create') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-edit-32"></div>
            </div>
            <span>New Article</span>
        </a>
    </li>
    <li class="selected">
        <a href="{{ route('articles.my-articles') }}">
            <div class="icon-w">
                <div class="os-icon os-icon-file-text"></div>
            </div>
            <span>My Articles</span>
        </a>
    </li>
    <li class="sub-header">
        <span>Other</span>
    </li>
    <li class="selected">
        <a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Analytics</span>
        </a>
    </li>
    <li class="selected">
        <a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>Help</span>
        </a>
    </li>
    <li class="selected">
        <a href="#">
            <div class="icon-w">
                <div class="os-icon os-icon-layout"></div>
            </div>
            <span>SEO</span>
        </a>
    </li>
</ul>
</div>
