@extends('layouts.auth-master')

@section('content')
<div class="all-wrapper menu-side with-pattern">
    <div class="auth-box-w wider">
        <div class="logo-w">
            <a href="index.html"><img alt="" src="img/logo-big.png"></a>
        </div>
        <h4 class="auth-header">Create new account</h4>
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group">
                <label for="username">Username</label>
                <input id="username" type="text" class="form-control {{ $errors->has('username') ? 'is-invalid' : '' }}" name="username" placeholder="Enter your username" value="{{ old('username') }}" required autofocus>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="first_name"> First name</label>
                        <input id="first_name" type="text" class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}" name="first_name"  placeholder="Enter your first name" required>
                        <div class="pre-icon os-icon os-icon-fingerprint"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="last_name">Last name</label>
                        <input id="last_name" type="text" class="form-control" name="last_name" placeholder="Enter your last name" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="email"> Email address</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Enter email" value="{{ old('email') }}" required>
                <div class="pre-icon os-icon os-icon-email-2-at2"></div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password"> Password</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                        <div class="pre-icon os-icon os-icon-fingerprint"></div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="password-confirm">Confirm Password</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Password" required>
                    </div>
                </div>
            </div>
            <div class="buttons-w">
                <button class="btn btn-primary">Register Now</button>
            </div>
        </form>
    </div>
</div>
@endsection
