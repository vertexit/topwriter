@extends('layouts.master')
@section('content')

<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <h6 class="element-header">All Permissions</h6>
            <div class="element-box">
                <h5 class="form-header">All Permissions</h5>
                <div class="table-responsive">
                    <table id="permissions-datatable" width="100%" class="table table-striped table-lightfont">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Roles that have this permission</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Roles that have this permission</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')

<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#permissions-datatable").DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: "{{ route('permissions.index') }}",
            columns: [
                { data: "id" },
                { data: "name" },
                {
                    data: "roles",
                    searchable: false,
                    orderable: false
                },
                { data: "action" }
            ],
            columnDefs: [
                {
                    targets: 2,
                    render: function (data) {
                        permissions = [];
                        $.each(data, function (i, permission) {
                            permissions.push(permission.name);
                        });

                        return permissions.join(', ');
                    }
                }
            ],
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();
});

</script>

@endsection
