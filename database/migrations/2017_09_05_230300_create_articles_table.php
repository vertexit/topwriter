<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('content')->nullable();
            $table->string('image')->nullable(); // 3 sizes
            $table->integer('views')->default(0);
            $table->integer('upvotes')->default(0);
            $table->integer('downvotes')->default(0);
            $table->string('status')->default('offline'); // pinned, online, offline, review, draft, approved, rejected
            $table->integer('user_id');
            $table->integer('category_id');
            $table->integer('theme_id');
            $table->timestamps();
        });

        Schema::create('articles_edited', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('article_id');
            $table->text('content');
            $table->unsignedInteger('user_id');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('article_id')->references('id')->on('articles');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
