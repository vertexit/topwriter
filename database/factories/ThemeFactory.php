<?php

use Faker\Generator as Faker;

$factory->define(App\Theme::class, function (Faker $faker) {
    $name = 'Top 10 ' . $faker->sentence($nbWords = 5);

    return [
        'name' 			=> $name,
        'slug' 			=> str_slug($name),
        'category_id' 	=> App\Category::inRandomOrder()->first()->id,
        // 'user_id'		=> App\User::where('id', '!=', 4)->inRandomOrder()->first()->id,
        'user_id'		=> 1,
        'status'		=> $faker->boolean ? 'approved' : 'pending',
    ];
});
