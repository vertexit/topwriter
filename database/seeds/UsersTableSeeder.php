<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
            'username' => 'admin',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin123'),
    	]);

        DB::table('model_has_roles')->insert([
            'role_id' => 1,
            'model_type' => 'App\User',
            'model_id' => 1,
        ]);

        DB::table('users')->insert([
            'username' => 'editor',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'editor@admin.com',
            'password' => bcrypt('admin123'),
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 2,
            'model_type' => 'App\User',
            'model_id' => 2,
        ]);

        DB::table('users')->insert([
            'username' => 'writer',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'writer@admin.com',
            'password' => bcrypt('admin123'),
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 3,
            'model_type' => 'App\User',
            'model_id' => 3,
        ]);

        DB::table('users')->insert([
            'username' => 'newbie',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'email' => 'newbie@admin.com',
            'password' => bcrypt('admin123'),
        ]);

        DB::table('model_has_roles')->insert([
            'role_id' => 4,
            'model_type' => 'App\User',
            'model_id' => 4,
        ]);

        factory(App\User::class, 50)->create();
    }
}
