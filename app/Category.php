<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use LogsActivity;

    protected static $logAttributes = ['category', 'subcategory'];

    protected $guarded = [];

    public function themes()
    {
    	return $this->hasMany('App\Theme');
    }

    public function articles()
    {
    	return $this->hasMany('App\Article');
    }
}
