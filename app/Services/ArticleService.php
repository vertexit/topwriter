<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Theme;
use App\Article;
use App\Services\SectionService;

class ArticleService
{
    /**
     * @var \App\Services\FileService
     */
    private $fileService;
    /**
     * @var \App\Services\SectionService
     */
    private $sectionService;

    /**
     * Creates new service instance
     *
     * @param  \App\Services\FileService    $fileService
     * @param  \App\Services\SectionService  $sectionService
     */
    public function __construct(FileService $fileService, SectionService $sectionService)
    {
        $this->fileService = $fileService;
        $this->sectionService = $sectionService;
    }

    /**
     * Public update method
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theme|null  $theme
     * @param  \App\Article|null  $article
     * @return mixed
     */
    public function update(Request $request, Theme $theme = null, Article $article = null)
    {
        return $this->processUpdate($request, $theme, $article);
    }

    /**
     * Checks request url and calls appropriate method
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theme|null  $theme
     * @param  \App\Article|null  $article
     * @return mixed
     */
    private function processUpdate(Request $request, Theme $theme = null, Article $article = null)
    {
        switch (Route::currentRouteName()) {
            case 'write.article':
                return $this->updateOrCreate($request, $theme, 'review');

            case 'write.draft':
            case 'write.autosave':
                return $this->updateOrCreate($request, $theme, 'draft');

            case 'write.image-upload':
                return $this->uploadImages($request, $theme);

            case 'articles.edited-after-published':
                return $this->updateAfterPublishing($request, $article);

            case 'articles.edited':
                return $this->approveAndUpdate($request, $article);

            case 'articles/update-approved/*':
                return $this->approveAndUpdatePublished($request, $article);
        }
    }

    /**
     * Updates existing 'draft' article or creates new article by writer
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theme  $theme
     * @param  string  $status
     * @return void
     */
    private function updateOrCreate(Request $request, Theme $theme, $status)
    {
        DB::transaction(function () use ($request, $theme, $status) {
            $article = Article::updateOrCreate([
                'slug' => Str::slug($theme->name)
            ],
            [
                'slug' => Str::slug($theme->name),
                'name' => $theme->name,
                'user_id' => Auth::id(),
                'category_id' => $theme->category_id,
                'theme_id' => $theme->id,
                'status' => $status,
            ]);

            $theme->update(['status' => $status]);

            foreach ($request->sections as $section) {
                //TODO: section['images'] izmijeniti sa $request->seo['images']
                //TODO: izdvojiti u funkciju
                //TODO: proslijediti $images u sectionService
                //TODO: preimenovati slike u na disku
                $images[] = null;
                foreach ($request->seo['images'] as $image) {
                    if ($image['order'] == $section['number']) {
                        array_push($images, $image);
                    }
                }

                $this->sectionService->updateOrCreate($article, $section);
            }

            // $article->update(['content' => $this->convertSectionsToHTML($article)]);
        });
    }

    /**
     * Stores uploaded images on disk
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theme  $theme
     * @return string
     */
    private function uploadImages(Request $request, Theme $theme)
    {
        $path =  $this->fileService->upload(
            $request->file,
            "articles/$theme->slug",
            $request->file->getClientOriginalName()
        );

        return file_path($path);
    }

    /**
     * Rejects|Approves and updates article by editor
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return void
     */
    private function approveAndUpdate(Request $request, Article $article)
    {
        DB::transaction(function () use ($request, $article) {
            foreach ($request->sections as $section) {
                $this->sectionService->updateOrCreate($article, $section);
            }

            $article->update([
                'content' => $this->convertSectionsToHTML($article),
                'status' => $request->status,
            ]);

            // TODO: ispraviti, ne moze approved biti kada je article approved
            $article->theme()->update(['status' => $request->status]);
        });
    }

    /**
     * Updates article by writer after it has been published
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return void
     */
    private function updateAfterPublishing(Request $request, Article $article)
    {
        DB::table('articles_edited')
            ->updateOrInsert([
                'article_id' => $article->id
            ],
            [
                'article_id' => $article->id,
                'content'    => 'preg_replace',
                'user_id'    => Auth::id()
            ]);

        $article->update(['status' => 'approved-edited']);

        // TODO: popraviti
        foreach ($request->sections as $section) {
            $this->sectionService->updateOrCreate($article, $section);
        }
    }

    /**
     * Rejects|Approves published article by editor and removes
     * edited version from "articles_edited" table
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return void
     */
    private function approveAndUpdatePublished(Request $request, Article $article)
    {
        if ($request->status == 'approved') {
            $article->update([
                'content' => preg_replace('#<script(.*?)>(.*?)</script>#is', '', $request->content),
                'status'  => 'approved'
            ]);
        } else {
            $article->update([
                'status'  => 'approved'
            ]);
        }

        DB::table('articles_edited')
            ->where('article_id', $article->id)
            ->delete();
    }

    /**
     * Creates html content for article from his sections
     *
     * @param  \App\Article  $article
     * @return string
     */
    private function convertSectionsToHTML(Article $article)
    {
        $sections[] = null;

        foreach ($article->sections as $section) {
            array_push(
                $sections,
                "<section><h2>$section->title</h2>",
                "$section->content</section>"
            );
        }

        return implode(preg_replace('#<script(.*?)>(.*?)</script>#is', '', $sections));
    }
}
