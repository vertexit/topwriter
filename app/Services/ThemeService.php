<?php

namespace App\Services;

use App\Theme;
use App\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

class ThemeService
{
    /**
     * [create description]
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function create(Request $request)
    {
        $category = Category::findOrFail($request->category);

        // If user is newbie and he does not have any themes
        if (Auth::user()->role === 0 && Auth::user()->themes->isEmpty()) {
            $status = 'approved';
        }

        // If editor or admin created theme
        if (Auth::user()->role === 2 || Auth::user()->role === 3) {
            $status = 'free';
        }

        Theme::create([
            'name' => $request->name,
            'slug' => str_slug($request->name),
            'category_id' => $category->id,
            'user_id' => Auth::id(),
            'status' => $status ?? 'pending',
            'picked_at' => date("Y-m-d H:i:s"),
        ]);
    }

    /**
     * [update description]
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theme  $theme
     * @return void
     */
    public function update(Request $request, Theme $theme)
    {
        switch (Route::currentRouteName()) {
            case 'themes.approve':
                return $this->approveTheme($theme, 'approved');

            case 'themes.reject':
                return $this->approveTheme($theme, 'rejected');

            case 'themes.get-free':
                return $this->getFreeTheme($theme);
        }
    }

    /**
     * Editor approves users theme
     *
     * @param  \App\Theme  $theme
     * @param  string  $status
     * @return void
     */
    private function approveTheme(Theme $theme, $status)
    {
        $theme->update([
            'status' => $status,
            'picked_at' => date("Y-m-d H:i:s"),
        ]);
    }

    /**
     * User gets free theme
     *
     * @param  Theme  $theme [description]
     * @return [type]        [description]
     */
    private function getFreeTheme(Theme $theme)
    {
        $theme->update([
            'status' => 'approved',
            'user_id' => Auth::id(),
            'picked_at' => date("Y-m-d H:i:s"),
        ]);
    }

    /**
     * Changes status of theme to 'free' or 'deleted'
     *
     * @param \App\Theme  $theme
     * @return void
     */
    public function destroy(Theme $theme)
    {
        switch (Route::currentRouteName()) {
            case 'themes.drop':
                $status = 'free';
                break;

            case 'themes.delete':
                $status = 'deleted';
                break;
        }

        $theme->update(['status' => $status]);
    }
}
