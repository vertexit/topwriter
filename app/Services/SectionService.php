<?php

namespace App\Services;
use App\Article;
use App\Section;

class SectionService
{
    /**
     * Creates or updates section
     *
     * @param  \App\Article  $article
     * @param  array  $section
     * @return void
     */
    public function updateOrCreate(Article $article, $section)
    {
        $images = null;

        if ($section['images']) {
            $images = json_encode(array_filter(explode('|', $section['images'])));
        }

        Section::updateOrCreate([
            'article_id' => $article->id,
            'order_number' => $section['number']
        ],
        [
            'article_id' => $article->id,
            'order_number' => $section['number'],
            'title' => $section['title'],
            'images' => $images,
            'videos' => $section['videos'] ?? null,
            'content' => $section['content'],
        ]);
    }
}
