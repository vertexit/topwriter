<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use App\Theme;
use App\Category;
use Yajra\DataTables\Facades\DataTables;

class ThemeCreateResponse implements Responsable
{
    public function toResponse($request)
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                Theme::query()
                    ->fromUser()
                    ->pending()
                    ->with('category')
            )->addColumn('action', function ($theme) {
                return view('themes.dt-action.new-theme', compact('theme'));
           })->make(true);
        }

        return view('themes.new-theme')->with(
            'categories', Category::all()
        );
    }
}
