<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use Yajra\DataTables\Facades\DataTables;
use App\Theme;

class ThemeIndexResponse implements Responsable
{
    public function toResponse($request)
    {
        return $this->processResponse($request);
    }

    private function processResponse($request)
    {
        switch ($request->path()) {
            case 'themes':
                return $this->getAllThemes();

            case 'free-themes':
                return $this->getFreeThemes();

            case 'write':
                return $this->getUsersThemes();
        }
    }

    private function getAllThemes()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Theme::query()
                        ->with('category')
                        ->latest()
                )->addColumn('action', function ($theme) {
                    return view('themes.dt-action.all-themes', compact('theme'));
               })->make(true);
        }

        return view('themes.all-themes');
    }

    private function getFreeThemes()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                Theme::query()
                    ->free()
                    ->with('category')
            )->addColumn('action', function ($theme) {
                return view('themes.dt-action.free-themes', compact('theme'));
           })->make(true);
        }

        return view('themes.free-themes');
    }

    private function getUsersThemes()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Theme::query()
                        ->fromUser()
                        ->approved()
                        ->with('category')
                )->addColumn('action', function ($theme) {
                    return view('themes.dt-action.my-themes', compact('theme'));
               })->make(true);
        }

        return view('themes.my-themes');
    }
}
