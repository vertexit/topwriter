<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use App\Theme;
use App\Article;

class ArticleCreateResponse implements Responsable
{
    public function __construct(Theme $theme)
    {
        $this->theme = $theme;
    }

    public function toResponse($request)
    {
        return view('articles.write')->with([
            'theme' => $this->theme,
            'sections' => $this->getSections(),
        ]);
    }

    /**
     * Gets all related sections if article exist
     *
     * @return mixed
     */
    private function getSections()
    {
        $article = Article::where('theme_id', $this->theme->id)
            ->first();

        if (! $article) {
            return null;
        }

        return $article->sections;
    }

}
