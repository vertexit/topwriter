<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Article;
use SebastianBergmann\Diff\Diff;

class ArticleUpdateResponse implements Responsable
{
    /**
     * Creates new response instance
     *
     * @param  mixed  $response
     */
    public function __construct($response)
    {
        $this->response = $response;
    }

    public function toResponse($request)
    {
        return $this->processResponse($request);
    }

    private function processResponse($request)
    {
        switch (Route::currentRouteName()) {
            case 'article/*':
            case 'articles/continue-edit/*':
                return $request->ajax() ? 'true' : redirect('home/my-articles');

            case 'articles/edit/*':
            case 'articles/update-approved/*':
                return redirect('home/articles');

            case 'write/draft*/':
                return response()->json('saved');

            case 'write.image-upload':
                return $this->response;

            case 'write/autosave/*':
                return response()->json('success');
        }
    }

}
