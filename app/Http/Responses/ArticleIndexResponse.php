<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Article;

class ArticleIndexResponse implements Responsable
{
    public function __construct()
    {

    }

    public function toResponse($request)
    {
        return $this->processRequest($request);
    }

    private function processRequest($request)
    {
        switch (Route::currentRouteName()) {
            case 'articles.all-articles':
                return $this->getAllArticles();

            case 'articles.my-articles':
                return $this->getUsersArticles();
        }
    }

    private function getAllArticles()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                Article::query()
                    ->with('category')
                    ->latest()
            )->addColumn('action', function ($article) {
                return view('articles.dt-action.all-articles', compact('article'));
           })->make(true);
       }

        return view('articles.all-articles');
    }

    private function getUsersArticles()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                Article::query()
                    ->where('user_id', Auth::id())
                    ->with('category')
            )->addColumn('action', function ($article) {
                return view('articles.dt-action.my-articles', compact('article'));
           })->make(true);
        }

        return view('articles.my-articles');
    }

}
