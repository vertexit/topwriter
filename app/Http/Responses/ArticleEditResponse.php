<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use App\Article;
use SebastianBergmann\Diff\Diff;

class ArticleEditResponse implements Responsable
{
    public function __construct(Article $article)
    {
        $this->article = $article;
        $this->status = $article->status;
        $this->editedArticle = $this->getEditedArticle();
    }

    public function toResponse($request)
    {
        // $articleEdited = null;
        // $diff = null;

        // if ($article->status == 'approved-edited') {
            // $articleEdited = DB::table('articles_edited')
            //                     ->where('article_id', $article->id)
            //                     ->first();

            // $fineDiff = new Diff;
            // $diff = $fineDiff->render(
            //     trim(str_replace('&nbsp;', ' ', $article->content)),
            //     trim(str_replace('&nbsp;', ' ', $articleEdited->content))
            // );
        // }

        // $images = $this->readImagesFromFolder($article->slug);

        // $theme = $article->theme;

        // return view('admin.write', compact('article', 'articleEdited', 'diff', 'images', 'theme'));
        return view('articles.write')->with([
            'article' => $this->article,
            'sections' => $this->article->sections,
            'editedArticle' => $this->editedArticle,
            'diff' => $this->getDiff(),
            // 'images' => $this->getImages(),
            'theme' => $this->article->theme,
        ]);
    }

    private function getEditedArticle()
    {
        if ($this->status != 'approved-edited') {
            return null;
        }

        return DB::table('articles_edited')
            ->where('article_id', $this->article->id)
            ->first();
    }

    private function getDiff()
    {
        if ($this->status != 'approved-edited') {
            return null;
        }

        // TODO: napraviti diff
        return new Diff(
            trim(str_replace('&nbsp;', ' ', $this->article->content)),
            trim(str_replace('&nbsp;', ' ', $this->editedArticle->content))
        );

        // return $fineDiff->render(
        //     trim(str_replace('&nbsp;', ' ', $this->article->content)),
        //     trim(str_replace('&nbsp;', ' ', $this->editedArticle->content))
        // );
    }

    private function getImages()
    {
        // TODO: promijeniti
        return $this->readImagesFromFolder($article->slug);
    }

}
