<?php

if (! function_exists('file_path')) {
    /**
     * TODO: da radi za sve fajlove
     * TODO: ako je slika da izbaci 404 sliku
     * TODO: dodati u config i env putanju do 404 slike
     *
     * @param  string $path
     * @return string
     */
	function file_path($path) {
		if ($path) {
			return url(Storage::url($path));
		}

		return asset('theme/images/404.jpg');
	}
}
