<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class FilterRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        // if user is guest send to login page
        if (Auth::guest()) {
            return redirect('/login');
        }

        // if user has lesser role abort
        if (Auth::user()->role < $role) {
            abort(403);
        }

        return $next($request);
    }
}
