<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|max:191|email',
            'photo' => 'nullable|image',

            'about_me'  => 'nullable|string',
            'facebook'  => 'nullable|string|url',
            'instagram' => 'nullable|string|url',
            'twitter'   => 'nullable|string|url',
            'youtube'   => 'nullable|string|url',
        ];
    }
}
