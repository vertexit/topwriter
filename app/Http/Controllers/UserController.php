<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * New controller instance
     */
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                User::query()
                    ->withCount('articles', 'themes')
                    ->with('roles', 'permissions')
                    ->orderBy('created_at', 'desc')
                )->addColumn('action', function ($user) {
                    return view('users.dt-action.all-users', compact('user'));
               })->make(true);
        }

        return view('users.all-users');
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return view('admin.profile')
            ->withUser($request->user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request)
    {
        $request->user()
            ->update($request->all());

        return back()->withStatus('Uspjesno ste izmjenili podatke na profilu!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
