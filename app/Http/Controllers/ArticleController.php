<?php

namespace App\Http\Controllers;

use App\Article;
use App\Theme;
use Illuminate\Http\Request;
use App\Http\Responses;
use App\Services\ArticleService;

class ArticleController extends Controller
{
    /**
     * @var \App\Services\ArticleService
     */
    private $articleService;

    /**
     * New controller instance
     *
     * @param  \App\Services\ArticleService  $articleService
     */
    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Responses\ArticleIndexResponse
     */
    public function index()
    {
        return new Responses\ArticleIndexResponse;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Theme  $theme
     * @return \App\Http\Responses\ArticleCreateResponse
     */
    public function create(Theme $theme)
    {
        return new Responses\ArticleCreateResponse($theme);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \App\Http\Responses\ArticleEditResponse
     */
    public function edit(Article $article)
    {
        return new Responses\ArticleEditResponse($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theme|null  $theme
     * @param  \App\Article|null  $article
     * @return \App\Http\Responses\ArticleUpdateResponse
     */
    public function update(Request $request, Theme $theme = null, Article $article = null)
    {
        $response = $this->articleService->update($request, $theme, $article);

        return new Responses\ArticleUpdateResponse($response);
    }
}
