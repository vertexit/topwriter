<?php

namespace App\Http\Controllers;

use App\Http\Requests\ThemeRequest;
use App\Http\Responses;
use App\Theme;
use App\Services\ThemeService;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    /**
     * @var \App\Services\ThemeService
     */
    private $themeService;

    /**
     * Creates new instance
     *
     * @param  \App\Services\ThemeService  $themeService
     */
    public function __construct(ThemeService $themeService)
    {
        $this->themeService = $themeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Responses\ThemeIndexResponse
     */
    public function index()
    {
        return new Responses\ThemeIndexResponse;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \App\Http\Responses\ThemeCreateResponse
     */
    public function create()
    {
        return new Responses\ThemeCreateResponse;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ThemeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ThemeRequest $request)
    {
        $this->themeService->create($request);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Theme  $theme
     * @return void
     */
    public function update(Request $request, Theme $theme)
    {
        $this->themeService->update($request, $theme);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Theme  $theme
     * @return void
     */
    public function destroy(Theme $theme)
    {
        $this->themeService->destroy($theme);
    }

}
