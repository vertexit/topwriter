<?php

namespace App;

use App\Services\FileService;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use LogsActivity;
    use Notifiable;
    use HasRoles;

    public function getRouteKeyName()
    {
        return 'username';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'password',
        'email',
        'photo',
        'about_me',
        'facebook',
        'instagram',
        'twitter',
        'youtube',
    ];

    protected static $logFillable = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getMainRoleAttribute()
    {
        $roles = $this->getRoleNames()->toArray();

        switch ($roles) {
            case in_array('super admin', $roles):
                return 'Super Admin';
            case in_array('editor', $roles):
                return 'Editor';
            case in_array('writer', $roles):
                return 'Writer';
            case in_array('newbie', $roles):
                return 'Newbie';
        }
    }

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function themes()
    {
        return $this->hasMany('App\Theme');
    }

    public function notifications()
    {
        return $this->belongsToMany('App\Notification')->withPivot('seen');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    // public function analytics()
    // {
    //     return $this->hasOne('App\Analytics');
    // }
    //
    // public function discussions()
    // {
    //     return $this->belongsToMany('App\Discussion');
    // }
    //
    // public function experience()
    // {
    //     return $this->hasMany('App\Point');
    // }

    /**
     *
     * Check if user owns related model
     *
     */
    public function owns($related)
    {
        return $this->id === $related->user_id;
    }
}
