<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Theme extends Model
{
    use LogsActivity;

    protected static $logAttributes = ['name', 'user_id', 'status', 'picked_at'];

    protected $guarded = [];

    public function getRouteKeyName()
	{
	    return 'slug';
	}

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeApproved($query)
    {
    	return $query->where('status', 'approved');
    }

    public function scopeDraft($query)
    {
        return $query->where('status', 'draft');
    }

    public function scopePending($query)
    {
    	return $query->where('status', 'pending');
    }

    public function scopeFree($query)
    {
        return $query->where('status', 'free');
    }

    public function scopeFromUser($query)
    {
        return $query->where('user_id', Auth::user()->id);
    }
}
