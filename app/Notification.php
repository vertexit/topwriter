<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $guarded = ['id', 'seen'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
