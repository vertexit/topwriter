<?php

namespace App\Policies;

use App\User;
use App\Theme;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThemePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Checks if user owns theme to delete
     *
     * @param  User   $user  [description]
     * @param  Theme  $theme [description]
     * @return [type]        [description]
     */
    public function destroy(User $user, Theme $theme)
    {
        return $user->owns($theme);
    }
}
