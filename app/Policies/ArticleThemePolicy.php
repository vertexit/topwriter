<?php

namespace App\Policies;

use App\User;
use App\Theme;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ArticleThemePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Checks if user owns the theme he wants to write article about
     * and if the theme is not online or offline
     *
     * @param  User   $user  [description]
     * @param  Theme  $theme [description]
     * @return [type]        [description]
     */
    public function create(User $user, Theme $theme)
    {
        if ($user->owns($theme) && ! in_array($theme->status, ['online', 'offline'])) {
            return true;
        }

        return false;
    }
}
