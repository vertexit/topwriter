<?php

namespace App\Policies;

use App\User;
use App\Article;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Editor can edit only if article status is
     * review, approved or approved-edited
     *
     * @param  Article $article [description]
     * @return [type]           [description]
     */
    public function edit(User $user, Article $article)
    {
        if (in_array($article->status, array('review', 'approved', 'approved-edited'))) {
            return true;
        }

        return false;
    }
}
