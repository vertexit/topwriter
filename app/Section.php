<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $guarded = [];

    public function article()
    {
        return $this->belongsTo('App\Article');
    }
}
