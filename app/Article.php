<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Article extends Model
{
    use LogsActivity;

    protected static $logAttributes = ['name', 'content', 'image', 'status', 'user_id'];

	protected $guarded = [];

	public function getRouteKeyName()
	{
	    return 'slug';
	}

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function theme()
    {
        return $this->belongsTo('App\Theme');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function sections()
    {
        return $this->hasMany('App\Section');
    }
}
