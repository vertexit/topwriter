<?php

Auth::routes();

// Admin panel routes
Route::group(['middleware' => 'auth'], function () {
    // dashboard
    Route::get('/', 'HomeController@dashboard')->name('dashboard');

     // create
    Route::get('write', 'ThemeController@index')->name('themes.create');
    Route::post('theme', 'ThemeController@store')->name('themes.store');
    Route::get('free-themes', 'ThemeController@index')->name('themes.free');
    Route::get('new-theme', 'ThemeController@create')->name('themes.new');

    // help
    Route::get('help', 'HomeController@help');

    // Discussion
    Route::get('discussion', 'HomeController@discussion')->name('discussion.index');
    Route::get('discussion2', 'HomeController@discussion2')->name('discussion.index-2');

    Route::group(['prefix' => 'write'], function() {
        // start writing article
        Route::get('article/{theme}', 'ArticleController@create')
            ->name('write');
            // ->middleware('can:create,theme');

        Route::post('article/{theme}', 'ArticleController@update')->name('write.article');

        Route::post('autosave/{theme}', 'ArticleController@update')
            ->name('write.autosave');

        // save images to disk
        Route::post('upload-image/{theme}', 'ArticleController@update')
            ->name('write.image-upload');

        // save as draft
        Route::post('draft/{theme}', 'ArticleController@update')->name('write.draft');
    });

    // notifications
    Route::get('notifications', 'HomeController@notifications');

    // notifications seen / unseen
    Route::post('notificationSeenUnseen', 'HomeController@notiSeenUnseen')
        ->name('notiSeenUnseen');

    ############################## WRITER | EDITOR | ADMIN
    // Route::group(['middleware' => 'role:1'], function () {
        // user profile
        Route::get('profile', 'UserController@show');
        Route::post('profile', 'UserController@update');

        // my articles
        Route::get('my-articles', 'ArticleController@index')
            ->name('articles.my-articles');

        // comments
        Route::get('comments', 'HomeController@comments');

        // analytics
        Route::get('analytics', 'HomeController@analytics');

        // get free theme
        Route::put('themes/get-free/{theme}', 'ThemeController@update')->name('themes.get-free');

        // drop existing theme
        Route::delete('themes/drop/{theme}', 'ThemeController@destroy')->name('themes.drop');

        // delete proposed theme
        Route::delete('themes/delete/{theme}', 'ThemeController@destroy')
            ->name('themes.delete')
            ->middleware('can:destroy,theme');

        // edit article after it has been published
        Route::get('articles/edit-after-published/{article}', 'ArticleController@edit')->name('articles.edit-after-published');
        Route::put('articles/edit-after-published/{theme}/{article}', 'ArticleController@update')->name('articles.edited-after-published');

        ####################################### EDITOR | ADMIN
        // Route::group(['middleware' => 'role:2'], function () {

            Route::group(['prefix' => 'articles'], function() {
                // articles
                Route::get('/', 'ArticleController@index')
                    ->name('articles.all-articles');

                // edit article
                Route::get('edit/{article}', 'ArticleController@edit')
                    ->name('articles.edit')
                    ->middleware('can:edit,article');

                // reject / edit and approve article
                Route::put('edit/{theme}/{article}', 'ArticleController@update')->name('articles.edited');

                // reject / edit and approve online edited article
                Route::post('update-approved/{article}', 'ArticleController@update');
            });

            // all themes
            Route::get('themes', 'ThemeController@index')
                ->name('themes.all-themes');

            // approve | reject theme
            Route::put('themes/approve/{theme}', 'ThemeController@update')
                ->name('themes.approve');
            Route::put('themes/reject/{theme}', 'ThemeController@update')
                ->name('themes.reject');

            Route::get('categories', 'CategoryController@index')
                ->name('categories.all-categories');

            Route::post('category/create', 'CategoryController@store');

            Route::post('category/update/{category}', 'CategoryController@update');

            ################################################ ADMIN
            // Route::group(['middleware' => 'role:3'], function () {
                // users
                Route::get('users', 'UserController@index')
                    ->name('users.index')
                    ->middleware('can:user read');

                // activity log
                Route::get('activity-log', 'HomeController@activity')
                    ->name('activity-log');

                // user profile
                Route::get('users/{user}', 'HomeController@user');

                // advanced-analytics
                Route::get('advanced-analytics','HomeController@advancedAnalytics');

                Route::get('roles', 'RoleController@index')->name('roles.index');

                Route::get('permissions', 'PermissionController@index')->name('permissions.index');

                // update role
                Route::post('update/role', 'HomeController@updateRole')
                    ->name('updateRole');
            // });
        // });
    // });
});
